﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class NewBehaviourScript : MonoBehaviour {

	public List<Ingredient> Ingredients = new List<Ingredient>();
	public Button submit;
	public Canvas canvas;
	public Toggle tog;

	// Use this for initialization
	public void Start () {
		Button btn = submit.GetComponent<Button> ();
		btn.onClick.AddListener (TaskOnClick); 
	}
		
	
	// Update is called once per frame
	void Update () {
	}
		
	void TaskOnClick(){
		if (tog.isOn) {
			Ingredients.Add (new Ingredient (gameObject.name));
			for (int i = 0; i < Ingredients.Count; i++) {
				print (Ingredients [i].Ing);
			}
		}
	}

	[System.Serializable]

	public class Ingredient
	{
		public string Ing;
		public Ingredient(){
	}
		public Ingredient (string ing){
			Ing = ing;
		}
	} 
}
